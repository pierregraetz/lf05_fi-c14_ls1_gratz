
public class Ausgabeformatierung1 {

	public static void main(String[] args) {
		String name = "Fritz";
		System.out.println(name + " f�ngt frische Fische.\n \"Frische Fische\" f�ngt " + name + ".\n\n");
		//print() gibt einen String ohne anschlie�enden Zeilenbruch aus, println() enth�lt diesen Zeilenumbruch
		
		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***");
		System.out.println("     ***");
		
		
		
		System.out.printf( "%.2f\n" , 22.4234234);
		System.out.printf( "%.2f\n" , 111.2222);
		System.out.printf( "%.2f\n" , 4.0);
		System.out.printf( "%.2f\n" , 1000000.551);
		System.out.printf( "%.2f\n" , 97.34);

	}

}
