import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geschlecht [w]/[m]: ");
		char gender = sc.next().charAt(0);
		System.out.print("Gewicht in kg: ");
		double weight = sc.nextDouble();
		System.out.print("Gr��e in cm: ");
		double height = sc.nextDouble();
		
		String bmi = calculate(gender, weight, height);
		System.out.println(bmi);
	}
	
	public static String calculate(char gender, double weight, double height) {
		double heightInM = height / 100;
		double bmi = weight / (heightInM * heightInM);
		
		System.out.println(bmi);
		if (gender == 'w') {
			if (bmi < 19) {
				return "Untergewicht";
			}
			else if (bmi <= 24) {
				return "Normalgewicht";
			}
			else {
				return "�bergewicht";
			}
		}
		else {
			if (bmi < 20) {
				return "Untergewicht";
			}
			else if (bmi <= 25) {
				return "Normalgewicht";
			}
			else {
				return "�bergewicht";
			}
		}
	}

}
