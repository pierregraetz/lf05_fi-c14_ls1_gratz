import java.util.Scanner;

public class Funktionslöser {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double result;
		System.out.print("Wert x: ");
		double x = sc.nextDouble();
		
		if (x <= 0) {
			result = Math.pow(2.718, x);
			System.out.println("f(" + x + ") = " + result + "\nexponentiell");
		}
		else if (x > 0 && x <= 3) {
			result = Math.pow(x, 2) + 1;
			System.out.println("f(" + x +") = " + result + "\nquadratisch");
		}
		else {
			result = 2 * x + 4;
			System.out.println("f(" + x +") = " + result + "\nlinear");
		}

	}

}
