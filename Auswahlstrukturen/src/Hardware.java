import java.util.Scanner;

public class Hardware {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Anzahl der M�use: ");
		int anzahl = sc.nextInt();
		System.out.print("Einzelpreis: ");
		double preis = sc.nextDouble();
		double gesamt = berechne(anzahl, preis);
		System.out.println("Gesamtpreis: " + gesamt);
	}
	
	public static double berechne(int x, double y) {
		double gesamt;
		boolean versandkosten;
		double wert = x * y;
		if (x >= 10) {
			versandkosten = false;
		}
		else {
			versandkosten = true;
		}
		
		if (0 < wert && wert < 100) {
			wert = wert * 0.9;
		}
		else if (100 < wert && wert < 500) {
			wert = wert * 0.85;
		}
		else {
			wert = wert * 0.8;
		}
		
		if (versandkosten) {
			gesamt = wert * 1.19 + 10;
		}
		else {
			gesamt = wert * 1.19;
		}
		return gesamt;
	}

}
