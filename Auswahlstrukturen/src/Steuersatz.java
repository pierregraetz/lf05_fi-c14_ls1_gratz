import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double total;
		System.out.print("Geben Sie den Nettowert ein: ");
		double value = sc.nextInt();
		System.out.print("Steuersatz erm��igt [j] oder voll [n]? ");
		char answer = sc.next().charAt(0);
		
		if (answer == 'j') {
			total = value + value * 0.07;
			System.out.println("Gesamt: " + total);
		}
		else {
			total = value + value * 0.19;
			System.out.println("Gesamt: " + total);
		}

	}

}
