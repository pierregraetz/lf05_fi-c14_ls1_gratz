﻿import java.util.Scanner;

class Fahrkartenautomat
{

    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);
        boolean repeat = true;
        while(repeat) {
        	double preis = fahrkartenbestellungErfassen(tastatur);
        	double gesamtbetrag = geldeinwurf(preis, tastatur);
        	fahrscheinausgabe();
        	rückgeld(preis, gesamtbetrag);
        	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.\n\n");
        }
    }

    public static double fahrkartenbestellungErfassen(Scanner sc) {
    	double zuZahlenderBetrag = 0;
    	double zwischensumme = 0;
    	boolean repeat = true;
    	int wahlTicket;
    	int anzahl = 0;
    	String[] fahrkarten = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
    			"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
    			"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenPreise = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};
    	
    	//Vorteil von Arrays: Ermöglichen einfacheres Verändern der Daten
    	
    	while(repeat) {
    		System.out.println("Wählen Sie Ihre Wunschfahrkarte aus:");
    		for (int i = 0; i < fahrkarten.length; i++) {
    			System.out.printf("%s [%.2f] (%d)\n", fahrkarten[i], fahrkartenPreise[i], i + 1); 
    		}
    		System.out.println("\nBezahlen (0)");
    		do {
    			System.out.print("Ihre Wahl: ");
    			wahlTicket = sc.nextInt();
    			if(wahlTicket > 0 && wahlTicket < fahrkarten.length) {
    				for(int i = 0; i < fahrkarten.length; i++ ) {
    					if(wahlTicket == i + 1) {
    						zuZahlenderBetrag = fahrkartenPreise[i];
    					}
    				}
    			}
    			else if(wahlTicket == 0) {
    				return zwischensumme;
    			} else {
    				System.out.println(">>falsche Eingabe<<");
    			}
    		} while(wahlTicket < 0 || wahlTicket > fahrkarten.length);
        
    		while(anzahl < 1 || anzahl > 10) {
    			System.out.print("Anzahl der Tickets: ");
    			anzahl = sc.nextInt();
        
    			if (anzahl < 1 || anzahl > 10) {
    				System.out.println(">>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.<<");
    			}
    		}
    		zwischensumme = zwischensumme + zuZahlenderBetrag * anzahl;
    		anzahl = 0;
    		System.out.printf("Zwischensumme: %.2f EUR\n", zwischensumme);
    	}
    	return zwischensumme;
    }

    public static double geldeinwurf(double zuZahlenderBetrag, Scanner sc) {
        double eingezahlterGesamtbetrag = 0.0;
        double[] valideMuenzen = {0.05, 0.10, 0.20, 0.50, 1.00, 2.00};
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = sc.nextDouble();
            if (eingeworfeneMünze != 1 && eingeworfeneMünze != 2) {
                eingeworfeneMünze = eingeworfeneMünze / 100;
            }
            for (int i = 0; i < valideMuenzen.length; i++) {
            	if (eingeworfeneMünze == valideMuenzen[i]) {
            		eingezahlterGesamtbetrag += eingeworfeneMünze;
            		break;
            	}
            }
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrscheinausgabe() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                warte(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rückgeld(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro " ,rückgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "EURO");
                rückgabebetrag -= 2;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "EURO");
                rückgabebetrag -= 1;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "CENT");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "CENT");
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "CENT");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "CENT");
                rückgabebetrag -= 0.05;
            }
        }
    }
    public static void warte(int ms) throws InterruptedException {
        Thread.sleep(ms);
    }

    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + " " + einheit);
    }
}