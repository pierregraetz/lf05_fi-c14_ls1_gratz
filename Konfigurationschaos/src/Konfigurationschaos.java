
public class Konfigurationschaos {

	public static void main(String[] args) {
		final byte PRUEFNR = 4;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		double maximum = 100.00;
		double patrone = 46.24;
		int summe = muenzenCent + muenzenEuro * 100;
		int euro = summe / 100;
		int cent = summe % 100;
		double fuellstand = maximum - patrone;
		char sprachModul = 'd';
		String typ = "Automat AVR";
		String bezeichnung = "Q_2021_FAB_A";
		String name = typ + " " + bezeichnung;
		boolean statusCheck;
		
		statusCheck =
				(euro <= 150)
				&& (euro >= 50)
				&& (cent != 0)
				&& (fuellstand >= 50.00)
				&& (sprachModul == 'd')
				&& (!(PRUEFNR == 5 || PRUEFNR == 6));
		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pruefnummer : " + PRUEFNR);
		System.out.println("Fuellstand Patrone: " + fuellstand + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + statusCheck);
		

	}

}
