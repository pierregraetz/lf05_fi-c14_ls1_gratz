import java.util.Scanner;
public class Konsoleneingabe
{

 public static void main(String[] args)
 {

 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 double zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 double zahl2 = myScanner.nextInt();

 double ergebnisA = zahl1 + zahl2;
 double ergebnisS = zahl1 - zahl2;
 double ergebnisM = zahl1 * zahl2;
 double ergebnisD = zahl1 / zahl2;

 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisA);
 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisS);
 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisM);
 System.out.print("\n\n\nErgebnis der Division lautet: ");
 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisD);
 myScanner.close();
 }
}