import java.util.Scanner;

public class Fahrsimulator {

    public static void main(String[] args) {
        double v = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Beschleunigung in km/h: ");
        double dv = sc.nextDouble();
        v = beschleunige(v, dv);
        System.out.println("Neue Geschwindigkeit in km/h: " + v);
    }

    public static double beschleunige(double v, double dv) {
        v = v + dv;
        if (v >= 0 && v <= 130) {
            return v;
        }
        else {
            System.out.println("Ungültige Geschwindigkeit.");
            return 0;
        }

    }
}
