public class Folgen {
    public static void main(String[] args) {
        for (int i = 99; i > 0; i -= 3) {
            System.out.println(i);
        }
        int x = 1;
        for (int i = 1; i < 401; i += x) {
            System.out.println(i);
            x += 2;
        }
        for (int i = 2; i < 103; i += 4) {
            System.out.println(i);
        }
        int y = 4;
        for (int i = 4; i < 1025; i += y) {
            System.out.println(i);
            y += 8;
        }
        for (int i = 2; i < 32769; i *= 2) {
            System.out.println(i);
        }
    }
}
