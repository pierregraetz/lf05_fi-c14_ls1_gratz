public class Mathe {
    public static void main(String[] args) {
        System.out.println(quadrat(12));
        System.out.println(hypotenuse(7, 4));
    }

    public static double quadrat(double x) {
        return x * x;
    }

    public static double hypotenuse(double kathete1, double kathete2) {
        return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
    }
}
