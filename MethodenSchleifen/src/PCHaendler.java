import java.util.Scanner;

public class PCHaendler {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        // Benutzereingaben lesen
        String artikel = liesString("Was möchten Sie bestellen?", myScanner);

        int anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);

        double preis = liesDouble("Geben Sie den Nettopreis ein:", myScanner);

        double mwst = liesDouble("Geben SIe den Mehrwertsteuersatz in Prozent ein:", myScanner);

        // Verarbeiten
        double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
        double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

        // Ausgeben

        rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

    }

    public static String liesString(String text, Scanner sc) {
        System.out.println(text);
        return sc.next();
    }

    public static int liesInt(String text, Scanner sc) {
        System.out.println(text);
        return sc.nextInt();
    }

    public static double liesDouble(String text, Scanner sc) {
        System.out.println(text);
        return sc.nextDouble();
    }

    public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
        return anzahl * nettopreis;
    }

    public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
        return nettogesamtpreis * (1 + mwst / 100);
    }

    public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
    }

}
