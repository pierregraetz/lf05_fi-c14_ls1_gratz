import java.util.Scanner;

public class Quadrat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Quadratlaenge: ");
        int x = sc.nextInt();
        String asterisk = "* ";
        String repeat = asterisk.repeat(x); //java 11 method
        for (int i = 0; i < 1; i++) {
            System.out.println(repeat);
            for (int j = 0; j < x - 2; j++) {
                System.out.println(asterisk + "  ".repeat(x - 2) + asterisk);
            }
            System.out.println(repeat);
        }
    }
}
