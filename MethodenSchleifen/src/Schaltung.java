public class Schaltung {
    public static void main(String[] args) {
        double r1 = 10;
        double r2 = 20;
        System.out.println("Ersatzwiderstand R: " + reihenschaltung(r1, r2));
        System.out.println("Parallelschaltung P: " + parallelschaltung(r1, r2));
    }

    public static double reihenschaltung(double r1, double r2) {
        return r1 + r2;
    }

    public static double parallelschaltung(double r1, double r2) {
        return 1/r1 + 1/r2;
    }
}
